# Guía de estilos Javascript

[Formato de código](FORMATO.md) | [Comentarios](COMENTARIOS.md) | [Nomenclatura](NOMENCLATURA.md) | Variables y Funciones | [Estructuras de control](ESTRUCTURAS-CONTROL.md) | [Buenas Prácticas](BUENAS-PRACTICAS.md)

## Variable y Funciones


### Variables

* Toda variable debe ser declarada antes de ser utilizada.
* Reducir al máximo el uso de variables globales.
* Se recomienda declarar todas las variables antes de escribir el código a ejecutar.
* Un bloque de variables deben ser declaradas un sólo keyword `var`
* Si se necesita comentar las variables, utilizar el mismo nivel de identación para todos los comentarios,
* No se recomienda declarar variables dentro de bucles `for`.

```javascript
var page,           // pagina actual
    total,          // total de paginas
    size = 10;      // cantidad de resultados por página
```




### Funciones

Las funciones o métodos son las estructuras de código mas potentes en Javascript por que es necesario seguir las siguientes reglas:

* Toda función debe ser declarada antes de ser utilizada.
* Primero se declaran las variables internas de la función y luego el código a ejecutar.
* No debe haber espacio entre el nombre de la función y inicio de parentesis.
* Todos los parámetros deben estar separados por un espacio.
* Debe haber un espacio entre el cierre de parentesis y el inicio de llaves.
* El cuerpo de la función debe tener un nivel de identación.
* Si se declara una función anonima, debe haber un espacio entre `function` y el inicio de llaves.
* Cuando una función se ejecuta inmediatamente se debe encerrar entre parentesis.
* Al ejecutar una función no debe haber espacio entre el nombre de la función y el inicio de parentesis.
* Se recomienda utilizar el modo estricto `stric mode` para asegurarse que no hay errores de sintaxis.


Ejemplos:

```javascript
// declaración de la función
function login(user, pass) {
    "strict mode";
    var error = "/login",
        success = "/home";
        
    if (user !== "" && pass !== "") {
        console.log(success);
    } else {
        console.log(error);
    }
}
```

```javascript
// utilizando función anónima
var login = function (user, pass) {
    "strict mode";
    var error = "/login",
        success = "/home";
        
    if (user !== "" && pass !== "") {
        console.log(success);
    } else {
        console.log(error);
    }
}

```


