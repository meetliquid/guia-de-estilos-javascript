# Guía de estilos Javascript

[Formato de código](FORMATO.md) | Comentarios | [Nomenclatura](NOMENCLATURA.md) | [Variables y Funciones](VARIABLES-FUNCIONES.md) | [Estructuras de control](ESTRUCTURAS-CONTROL.md) | [Buenas Prácticas](BUENAS-PRACTICAS.md)

## Comentarios

Los comentarios ayudan a la lectura del código, permiten entender 
la lógica del programador e incluso sirve para generar documentación.

En general los comentarios deben seguir las siguientes pautas:

* Siempre deben estar precedidas por una línea en blanco.
* Deben mantener el nivel de identación de la siguiente línea.

### Comentarios de una línea

Se utilizan para comentarios sencillos, aclaraciones rápidas, declaración de variables. 

```javascript
// Comentarios de una línea
```

* Si se usa al final del una línea de código: debe tener por lo menos un nivel de identación.
* En declaración de variables: deben identarse hasta mostrarse en la misma columna.
* No se debén utilizar de forma seguida.
* Sólo se permite utilizar de forma seguida cuando se comenta código.

### Comentarios de varias líneas

Si se requiere explicar con detalle un proceso o lógica que es muy larga, se recomienda el 
uso de comentarios en varias lineas.

```javascript
/*
 * Primera línea de comentarios
 * otra línea para detallar el código.
 */
```

* La primera línea siempre es: `/*` (*slash-asterisco*)
* La ultima línea siempre es: ` */` (*espacio-asterisco-slash*)
* Las líneas intermedias empiezan con: ` * ` (*espacio-asterisco-espacio*)
* Las líneas intermedas contienen los comentarios 


###  Comentarios para documentación 

Es posible utilizar los comentarios para documentar el Javascript, para ello se utiliza 
el formato JSDoc que es una variación de los comentarios de varias líneas.

```javascript
/**
 * Resumen del método o función a explicar.
 *
 * Descripción completa del método o función
 * se pueden utilizar varias líneas de código
 * y referencias para la lógica del código.
 * 
 * @param {type}   $var   Tipo y nombre del parametro
 * @param {type}   $var   Tipo y nombre del parametro
 * @returns {type}        Tipo y nombre que devuelve
 */
```

* La primera línea siempre es: `/**` (*slash-asterisco-asterisco*)
* La ultima línea siempre es: ` */` (*espacio-asterisco-slash*)
* Las líneas intermedias empiezan con: ` * ` (*espacio-asterisco-espacio*)
* La segunda línea es una descripción breve de lo que hace el código.
* Si se requiere agregar una descripción detallada, la seguna línea debe estar en blanco.
* Se pueden especificar parámetros con `@parameter`
* Los tipos de parámetros disponibles son:
    * `number` para valores numéricos
    * `string` para cadenas de texto 
    * `boolean` para valores booleanes
    * `Array` para matrices de datos
    * `object` para objetos javascript
    * `RegExp` para expresiones regulares
    * `function` para funciones
* Se puede especificar el valor devuelto con `@return`

Para mas información sobre el formato JSDoc pueden leer: [http://usejsdoc.org/](http://usejsdoc.org/)

### Herramientas

Pueden utilizar plugins para su editor de código preferido, por ejemplo:

* [FuncDocr](https://github.com/Wikunia/brackets-FuncDocr) para [Brackets](http://brackets.io/)
* [DocBlockr](https://packagecontrol.io/packages/DocBlockr) para [Sublime Text](http://www.sublimetext.com/)