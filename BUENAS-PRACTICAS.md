# Guía de estilos Javascript

[Formato de código](FORMATO.md) | [Comentarios](COMENTARIOS.md) | [Nomenclatura](NOMENCLATURA.md) | [Variables y Funciones](VARIABLES-FUNCIONES.md) | [Estructuras de control](ESTRUCTURAS-CONTROL.md) | Buenas Prácticas


## Buenas Prácticas

Las siguientes son recomendaciones generales para elaborar código que pueda ser mantenible en el tiempo y a travez 
de un equipo de programadores.

### Evitar el acoplamiento

El frontend esta compuesto de tres capas: *HTML*, *CSS* y *Javascript*, cada una con un rol específico. Si mezclamos 
estas capas se tiene el problema del acoplamiento que complica el mantenimiento y detección de errores.

**Mantener Javascript fuera del CSS**

Evitar el uso de expresiones para incluir Javascript en código CSS.

Ejemplo:
```css
/* Mala práctica */
.box {
    width: expression(document.body.offsetWidth + "px");
} 
```

**Mantener CSS fuera del Javascript**

Los estilos no deben estár ubicados en el código Javascript pues dificulta hacer los cambios visuales.

* Los nombres de clases, deben ser el método de comunicación entre CSS y Javascript.
* En javascript, evitar el uso de la propiedad `style`, en su lugar se recomienda el uso de `className`.
* En jQuery, evitar el uso del método `css`, en su lugar se recomienda el uso de `addClass`, `removeClass`.
* La excepción para aceptar el uso de `style` es cuando el calculo de posiciones no puede ser hecho en CSS.
* Se recomienda el uso de animaciones y transciones en CSS.

Ejemplos:

```javascript
// Mala práctica
element.style.color = "red";
element.style.top = "100px";

// Mala práctica
element.style.cssText = "color red; top: 10px;";

// Buena práctica
element.className += "error";
```

```javascript
// Mala práctica
$(element).css({color: "red", top: "100px"});

// Buena práctica
$(element).addClass("error");
```

**Mantener el Javascript fuera del HTML**

Si bien es posible incluir javascript dentro del HTML, se recomienda no escribir código javascript dentro del HTML.

* Todo el código javascript debe estar en archivos externos con extensión *.js*
* Se prohibe el uso de javascript dentro del HTML.
* Se prohibe hacer llamadas a funciones desde HTML.
* Sólo se permite código javascript en HTML para imprimir variables que vienen desde el servidor.

```html
<!-- Mala práctica -->
<button onclick="showPopup();" class="bt-terminos">Términos y condiciones</button>

<!-- Mala práctica -->
<script>showPopup();</script>
```

```javascript
// Buena práctica
$('.bt-terminos').on("click", showPopup);
```

** Mantener HTML fuera del Javascript**

Incluir HTML en javascript complica el mantenimiento del contenido, impide depurar fácilmente pues al inicio no es parte del DOM.

* Se recomienda no inyectar HTML desde Javascript.
* Se recomienda: tener bloques ocultos en HTML y mostrarlos posteriormente.
* Se recomienda: cargar HTML desde el servidor.
* Se recomienda: el uso de templates HTML y reemplazarlo con expresiones regulares.
* Se recomienda: el uso de un motor MVC.

Ejemplos:


```javascript
// Mala práctica
$('.div-error').html("<h3>Error</h3><p>Error en el formulario</p>");

// Buena práctica
$('.div-error').load("ajax/error.html");
```

### Evitar variables globales

Una variable se considera *global* cuando se puede acceder a ella desde cualquier código
Javascript. Para proyectos pequeños es una ventaja pero para trabajar en equipo es un gran problema: se generan conflictos con los nombres de variables, se generan conflictos con variables locales.

En general se tienen las siguientes recomendaciones.

* Siempre declarar las variables utilizando `var`
* Utilizar el modo estricto para depurar variables globales.
* Utilizar el mínimo de variables globales, por ejemplo en jQuery sólo se usa `$`
* Declarar variables dentro de funciones, objectos o clases.
* Utilizar *namespaces* para agrupar variables y funciones.
* Utilizar clases en javascript

Ejemplo: 

```javascript
// Namespace
var DB = {
    connect: function () {
        // init connection
    },
    query: function(sql) {
        // execute query
    },
    close: function() {
        // close connection
    }
};

DB.connect();
```

### Manejo de Eventos

En general el manejo de evento se hace sencillo con jQuery, pero podemos seguir las siguientes recomendaciones:

* Crear funciones para ejecutar los eventos.
* Separar el evento de la lógica de aplicación.
* Evitar pasar como parámetro el objeto event.

Esto permite: reutilizar código, hacer mas eficiente el mantenimiento y facilita las pruebas unitarias.

Ejemplo:


```javascript
// Generalmente
$('.button-terminos').on("click", function (e) {
    e.preventDefault();
    var $title = $(this).attr("data-title");
    
    $('.popup .title').text($title);
    $('.popup').addClass("show");
});
```

```javascript
// Recomendación
var onClickTerminos = function (e) {
    e.preventDefault();
    var $title = $(this).attr("data-title");
    showPopup($title);
}

var showPopup = function ($title) {    
    $('.popup .title').text($title);
    $('.popup').addClass("show");
});

$('.button-terminos').on("click", onClickTerminos);
```


### Separar datos de configuración

La configuración es cualquier valor escrito directamente en el código, los mas comunes son:

* URLs
* Cadenas de texto o mensajes que se muestran en el UI
* Valores repetidos
* Parámetros generales. 
* Cualquier valor que puede cambiar 

Se tienen las siguientes recomendaciones:

* Crear variables con estos valores.
* Crear constantes para valores que contienen parámetros generales.
* Crear un objeto que tenga todos estos valores.
* Colocarlos al inicio del código y comentarlos.
* Crea un archivo `config.js` que contenga estas variables.

Ejemplos: 

```javascript
// Generalmente
function loginUser(user) {
    if (user) {
        location.href = "/welcome.php";
    } else {
        showModal("Usuario incorrecto");
    }
}
```


```javascript
// Recomendación
var config = {
    URL_HOME_USER: "/welcome.php",          // URL Home del usuario loggeado
    MSG_INVALID_USER: "Usuario incorrecto"  // Mensaje para usuario incorrecto
}

function loginUser(user) {
    if (user) {
        location.href = config.URL_HOME_USER;
    } else {
        showModal(config.MSG_INVALID_USER);
    }
}
```

### Evitar modificar Objectos exsitentes

Javascript permite modificar cualquier objecto existente incluso los objectos nativos, esto
puede generar problemas de ejecución, se recomienda:

* No sobre-escribir métodos existentes
* No agregar nuevos métodos.
* No eliminar metodos existentes.
* Se recomienda crear clases que engloben nuevos métodos.
* Extender los objetos o clases existentes.
* Para objetos propios, utilizar los métodos `preventExtension`, `seal`, `freeze` para evitar
la sobreescritura. 
* Utilizar el modo estricto para asegurarnos que no estamos modificando objetos.

Ejemplos:


```javascript
// Mala práctica
document.getElementsByClassName = function(classes) {
    // implementación no-nativa 
};

// Mala práctica si usamos jQuery
var $ = function ($id) {
    return document.getElementById($id);
}

// Buena práctica
var DB = {
    connect: function () {
        // init connection
    },
    query: function (sql) {
        // execute query
    },
    close: function () {
        // close connection
    }
}
Object.freeze(DB);

```