# Guía de estilos Javascript

[Formato de código](FORMATO.md) | [Comentarios](COMENTARIOS.md) | [Nomenclatura](NOMENCLATURA.md) | [Variables y Funciones](VARIABLES-FUNCIONES.md) | [Estructuras de control](ESTRUCTURAS-CONTROL.md) | [Buenas Prácticas](BUENAS-PRACTICAS.md)


## Expresiones y Operadores 

Las expresiones permiten asignar variables o datos y los operadores ejecutar acciones básicas entre los datos.

### Expresiones

En general se deben seguir las siguientes reglas:

* Evitar la asignación de datos como parte de condicionales `if` y `while`.
* Las cadenas de texto siempre deben usar las comillas dobles `""`
* Para cadenas de texto largas usar concatenación en lugar de multiline.
* Si se usan número decimales siempre incluir la parte numérica y decimal.
* Las matrices deben inicializare utilizando los corchetes `[]`
* Los objetos deben inicializarse utilizando las llaves `{}`

Ejemplos:

```javascript
var nombre = 'Jim';                     // mal
var nombre = "Jim";                     // bien

var opacity = .5;                       // mal        
var opacity = 0.5;                      // bien

var colors = new Array("red", "blue");  // mal
var colors = ["red", "blue"];           // bien 

// Objeto mal inicializado
var book = new Object();
book.title = "Cien años de Soledad";
boot.author = "Gabriel García Marquez";

// Objeto bien inicializado
var book = {
    title: "Cien años de Soledad",
    author: "Gabriel García Marquez"
}
```



### Operadores

Se deben seguir las siguiente reglas:

* Los operadores deben estar separados de los operando por un espacio, excepto para el operador `.`
* Se recomienda el uso del comparador de igualdad `===` en lugar de `==`
* Se recomienda el uso del comparador de desigualdad `!==` en lugar de `!=`
* Se recomienda agrupar las operaciones con parentesis `()`
* Evitar el uso del operador incrementador `++` y decrementador `--`
* De prohibe el uso de `eval` para ejecutar javascript.


Ejemplos:

```javascript
var total = a+b;        // mal
var total = (a + b);    // bien

if (a == b) {           // mal
    // code
}
if (a === b) {          // bien
    // code
}

$total++;               // mal
$total = $total + 1;     // bien
```
