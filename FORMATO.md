# Guía de estilos Javascript

Formato de código | [Comentarios](COMENTARIOS.md) | [Nomenclatura](NOMENCLATURA.md) | [Variables y Funciones](VARIABLES-FUNCIONES.md) | [Estructuras de control](ESTRUCTURAS-CONTROL.md) | [Buenas Prácticas](BUENAS-PRACTICAS.md)

## Formato de código

### Espacios en blanco

Se deben emplear espacios en blanco en las siguientes ocasiones:

* Una *palabra-clave* seguida de un inicio de parentesis `(` deben separarse por un espacio.
* Al ejecutar una función no debe existir espacio entre la *palabra-clave* y el inicio de parentesis.
* La *palabra-clave* `function` siempre debe estar seguido por un espacio.
* Los operadores binarios deben estar separados de sus operandos por un espacio excepto el punto `.`
* Los operadores unarios no deben estar separados del operando excepto `typeof`
* Todas las comas `,` deben estar seguidas por un espacio o un salto de línea.
* Todos los punto y coma `;` al final una sentencia deben estar seguidas por un salto de línea.
* Todos los punto y coma `;` de un flujo `for` deben estar seguidos de un espacio en blanco.


### Identación

La identación es uno de los pricipales reglas para facilitar la lectura del código en un equipo, se deben seguir las siguientes pautas:

* Un nivel de identación debe ser de 4 espacios en lugar de tabs.
* Configurar el editor de código para que la hacer tab inserte 4 espacios.
* Se agrega un nivel de identación cuando la línea anterior termina en: `{`, `[`, `(`.
* Se agrega un nivel de identación si el primer caracter es un punto `.`
* Las palabras clave: `case`, `catch`, `default`, `else` no agregan nivel de identación.
* Si se declaran variables en varias líneas de debe agregar un nivel de identación.
* Si se utilizan varias líneas para una sentencia, se deben agregar dos niveles de identación a partir de la segunda línea.


### Terminación de línea

Para terminar una línea de código se deben seguir las siguientes reglas:

* Si se termina la sentencia se debe utilizar el punto y coma `;`
* Si se declaran o listan variables en varias líneas se debe terminar la línea con coma `,`

Es importante terminar las líneas con `;` para evitar problemas de interpretación de código.

### Líneas en blanco

Las líneas en blanco permiten separar bloques lógicos de código para facilitar la lectura. Se deben utilizar en los siguientes casos:

* Para separar métodos o funciones.
* Entre la declaración de variables locales y los métodos.
* Antes de un comentario
* Entres secciones lógicas dentro de un método para facilitar la lectura.


