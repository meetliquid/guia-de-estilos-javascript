# Guía de estilos Javascript

[Formato de código](FORMATO.md) | [Comentarios](COMENTARIOS.md) | Nomenclatura | [Variables y Funciones](VARIABLES-FUNCIONES.md) | [Estructuras de control](ESTRUCTURAS-CONTROL.md) | [Buenas Prácticas](BUENAS-PRACTICAS.md)

## Nomenclatura

Cada programador tiene formas de nombrar sus funciones, variables, constantes, pero con unos líneamientos vamos
a facilitar la lectura del código para trabajar en equipo.

### Archivos

Para la nomenclatura de los archivos se deben seguir las siguientes reglas:

* La extensión de los archivos siempre es `.js`
* Deben estar escritas completamente en minusculas.
* El único separador aceptado es el guión `-`.
* Nunca deben contener números.
* La carpeta de publicación debe ser `js`.

### Variables

Para la nomenclatura de variables se deben seguir las siguientes reglas:

* Siempre deben empezar con una letra en minusculas.
* Se debe utilizar nombres escritos en *camel case*.
* No contener verbos por ejemplo: `set`, `get`, `render`.
* Si la variable contiene un objeto del DOM debe empezar con `$`.
* Nombres de un solo caracter como `i`, `j`, `k` sólo se usan para bucles.
* De preferencia deben estar escritas en ingles.
* Deben ser lo mas cortas posibles.
* Deben describir el tipo de dato de ser posible.
* Evitar el uso de abreviaciones.

Ejemplos:

```javascript
var name;       // bien, contiene el nombre de usuario
var $popup;     // bien, contiene un objeto del DOM
var userName;   // bien, contiene una cadena con el nombre de usuario

var setName;    // mal, empieza con un verbo e indica una función
var isMobile;   // mal, esto indica una función
var test;       // mal, no indica que contiene
```

### Constantes

Las constantes sirven para definir datos de configuración y que no cambiarán en la ejecución del código. Si bien javascript no soporta 
constantes, utilizando una nomenclatura podemos identificar estos datos con las siguientes reglas:

* Deben estar escritar todas en mayusculas.
* El separador de palabras es el underline `_`.
* Deben declararse antes que las variables.
* Deben declararse y asignar el valor inmediatamente.
* De ser posible declararlos en un archivo de configuración.

Ejemplos:

```javascript
var EDX_PATH        = "http://campusromero.pe";
var EDX_CLIENT_ID   = "ac66699c40136c2709e8";
var MAX_COUNT       = 10;
```

### Funciones

Para la nomenclatura de funciones se deben seguir las siguientes reglas:

* Siempre deben empezar con una letra en minusculas.
* Se debe utilizar nombres escritos en *camel case*.
* Deben empezar con verbos, para ello se pueden tener:
    * `set`: función que guarda un valor.
    * `get`: función que devuelve un valor.
    * `is`: función que devuelve un valor booleno.
    * `has`: función que deveuelve un valor booleano.
    * `show`: función para mostrar un bloque HTML.
    * `hide`: función para ocultar un bloque HTML.
    * `init`: función para inicializar contenido.
    * `load`: función para cargar contenido remoto.
    * `on`: función para eventos
    * `render`: funciónpara calcular y mostrar contenido.
* De preferencia deben estar escritas en ingles.
* Deben ser lo mas cortas posibles.
* Deben describir la acción que ejecutan.
* Para método privados, pueden empezar con `_`.

Ejemplos: 

```javascript
setUserName("Jim"); // Asigna el nombre del usuario
isMobile();         // Detecta si el browser es un mobile
showPopup();        // Muestra el popup
onClickLogin();     // Ejecuta cuando se hace click en el login.
renderSlideshow();  // Calcula y muestra el slideshow
```

### Constructores o Clases

Los constructores o clases permiten agrupar lógicamente bloques de funciones para facilitar el entendimiento 
del código, se deben seguir las siguientes reglas.

* Siempre deben comenzar con una letra mayuscula.
* Se debe utilizar nombres escritos en *camel case*.
* Deben describir el tipo de objeto que contienen.
* No contener verbos por ejemplo: `set`, `get`, `render`.

Ejemplos:


```javascript
var user = new User();                  // Crea una nueva instanción de User
var db = new SQLiteDatabase();          // Crea una conexión a SQLite
var tooltip = new Tooltip("Welcome");   // Crea un objeto para los tooltips
```


