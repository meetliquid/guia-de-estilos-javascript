# Guía de estilos Javascript

[Formato de código](FORMATO.md) | [Comentarios](COMENTARIOS.md) | [Nomenclatura](NOMENCLATURA.md) | [Variables y Funciones](VARIABLES-FUNCIONES.md) | Estructuras de control | [Buenas Prácticas](BUENAS-PRACTICAS.md)

## Estructuras de control

Se consideran estructuras de control a las estructuras condicionales (`if`, `switch`) y las estructuras
repetitivas (`for`, `for...in`, `while`, `do`, `try`).

En general se deben seguir las siguientes reglas:

* El código a ejecutarse debe tener un nivel de identación adicional.
* El inicio de llaves debe ser seguido por una línea nueva.
* El final de llaves debe tener el mismo nivel de identación que el inicio.
* Se recomienda dejar una línea en blanco antes de cada estructura.

### Estructura `if...else`

Se deben seguir las siguiente reglas:

* El contenido de la estructura `if` y `else` debe tener un nivel de identación adicional.
* Siempre se debe agrupar el código a ejecutar con llaves `{}`
* Se prohibe el uso de condicionales ternarias.

Ejemplo:

```javascript
if (cout < MAX_COUNT) {
    showItem();
} else {
    return;
}
```

### Estructura `switch`

Se deben seguir las siguiente reglas:

* El contenido de la estructura `switch` debe tener un nivel de identación adicional.
* El contenido de cada sentencia `case` debe tener un nivel de identación adicional.
* Al final de cada sentencia `break` debe haber una línea en blanco.
* La sentencia `default` no es obligatoria.

Ejemplo:


```javascript
switch (condition) {
    case 1:
        // code
        break;
        
    case 2:
        // code
        break;
        
    default:
        // code
        break;
}
```

### Estructura `with`

La estructura `with` permite ejecutar sentencias dentro de un objeto convirtiendo las variables en locales.

* Se prohibe el uso de la estructura `with`
* Esta deshabilitado en el modo estricto.
* En su lugar se recomienda el uso de `for...in`

### Estructura `for`

Se deben seguir las siguientes reglas:

* Declarar la variable del contador antes de utilizarla.
* El contenido de la estructura `for` debe tener un nivel de identación adicional.
* No se permite el uso de `continue` para saltar la iteración actual.
* Se permite el uso de `break` para detener todas las iteraciones pendientes.

Ejemplo: 


```javascript
var i;

for (i = 0; i < MAX_COUNT; i = i +1 ) {
    showItem();
}
```

### Estructuras `for...in`

Estas estructuras se utlizan para recorrer los elementos de un objeto.

* Declarar la variable a usarse en los bucles.
* El contenido de la estructura `for...in` debe tener un nivel de identación adicional.
* Se recomienda el uso de `hasOwnProperty` para asegurarnos que no estamos recorriendo una propiedad heredada.

Ejemplo:


```javascript
var prop;

for (prop in object) {
    if (object.hasOwnProperty(prop)) {
        console.log("name: " + prop);
        console.log("value: "  + object[prop]);
    }
}
```

