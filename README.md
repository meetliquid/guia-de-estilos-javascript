# Guía de estilos Javascript

Una guía de estilos es un conjunto de reglas para escribir código de forma coherente y estandarizada. 

### Guía de estilos

Las reglas a seguir para redactar código son:

* [Formato de código](FORMATO.md)
* [Comentarios](COMENTARIOS.md)
* [Nomenclatura](NOMENCLATURA.md)
* [Variables y Funciones](VARIABLES-FUNCIONES.md)
* [Expresiones y Operadores](EXPRESIONES-OPERADORES.md)
* [Estructuras de control](ESTRUCTURAS-CONTROL.md)
* [Buenas Prácticas](BUENAS-PRACTICAS.md)

### JSLint

[JSLint](http://www.jslint.com/) es un analizador para detectar posibles inconsistencias en el formato de nuestro código.
Se puede utilizar de forma online o mediante el editor de código que ya lo traen integrado.

Es recomendable verificar siempre que nuestro código pasa el test de *JSLint*. Para facilitar el pase del test, se deben 
usar las siguientes recomendaciones:

* Al inicio del código indicar cuales son las variables globales, por ejemplo si estamos trabajando 
con jQuery podemos utilizar: `/*global $,console*/`
* Al inicio de cada función setear el modo escricto con: `"use strict";`

### Referencias

* Douglas Crockford Code Conventions [http://javascript.crockford.com/code.html](http://javascript.crockford.com/code.html)
* Maintainable Javascript [https://www.amazon.com/gp/product/1449327680/](https://www.amazon.com/gp/product/1449327680/)
* jQuery JavaScript Style Guide [https://contribute.jquery.org/style-guide/js/](https://contribute.jquery.org/style-guide/js/)
* Google JavaScript Style Guide [https://google.github.io/styleguide/jsguide.html](https://google.github.io/styleguide/jsguide.html)
* Dojo Style Guide [https://dojotoolkit.org/reference-guide/1.9/developer/styleguide.html](https://dojotoolkit.org/reference-guide/1.9/developer/styleguide.html)
